# smithers

A personal assistant app inspired by others' works including Jasper and such. This project is a test bed for me as I teach myself more API programming and refine my error handling and reusability.